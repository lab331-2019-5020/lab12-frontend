import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { CourseTableDataSource, CourseTableItem } from './course-table-datasource';
import { CourseServiceService } from 'src/app/service/course-service.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-course-table',
  templateUrl: './course-table.component.html',
  styleUrls: ['./course-table.component.css']
})
export class CourseTableComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<CourseTableItem>;
  dataSource: CourseTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['courseId', 'name', 'content', 'lecturer','view'];

  constructor(private courseService: CourseServiceService,private router: Router) { }

  ngOnInit() {
    this.courseService.getCourses().subscribe(
      courses => {
        this.dataSource = new CourseTableDataSource(this.paginator, this.sort);
        this.dataSource.data = courses;
      });
  }

  
  routeToCourseInfo(courseId: number) {
    this.router.navigate(['course/info',courseId]);
  }


}
