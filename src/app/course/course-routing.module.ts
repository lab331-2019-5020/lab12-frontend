import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CourseTableComponent } from './course-table/course-table.component';
import { CourseAddComponent } from './course-add/course-add.component';
import { CourseInfoComponent } from './course-info/course-info.component';

const courseRoutes: Routes = [
  { path: 'course/list', component: CourseTableComponent },
  { path: 'course/add', component: CourseAddComponent},
  { path: 'course/info/:id', component: CourseInfoComponent}
]; 

@NgModule({
  imports: [
    RouterModule.forRoot(courseRoutes)
  ],
  exports:[
    RouterModule
  ]
})
export class CourseRoutingModule { }
