import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Params, ActivatedRoute } from '@angular/router';
import Course from 'src/app/entity/course';
import { CourseServiceService } from 'src/app/service/course-service.service';

@Component({
  selector: 'app-course-info',
  templateUrl: './course-info.component.html',
  styleUrls: ['./course-info.component.css']
})
export class CourseInfoComponent implements OnInit {
  course: Course;
  constructor(private route: ActivatedRoute, private courseService:CourseServiceService) { }
  ngOnInit() {
    this.course = {
      'id': -1,
      'courseId': '',
      'content': '',
      'courseName': '',
      'lecturer': null
    };
    this.route.params
    .subscribe((params: Params) => {
      this.courseService.getCourse(+params['id'])
        .subscribe((inputCourse: Course) => this.course = inputCourse);
    });
  }
}
